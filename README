tuned: A simple daemon that performs monitoring and adaptive configuration
       of devices in the system

Copyright (C) 2008, 2009 Red Hat, Inc.
Authors: Phil Knirsch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


How to use
----------

After installing the package you simply need to start the tuned
service either with system-config-services or from the
commandline in a rootshell via:

  service tuned start

You can also automatically start it during system startup by
switching it on via chkconfig:

  chkconfig tuned on

After that the daemon will run automatically and monitor the
usage of various system components. Based on that information
components will then be put into lower or higher power saving
modes to adapt to the current usage. Currently only ethernet
network and and ATA harddisk devices are implemented.

The config file /etc/tuned.conf contains several sections in
INI style. There is always a "main" section which controls
overall options of the daemon. Currently only 1 option
is available:

interval:  Interval for monitoring and tuning in seconds.
           Default is 10.

Each monitoring and tuning plugin has an own section then.
Currently only 1 option for plugins is available:

enabled: Enabled or disable the plugin. Default is True.
         Any other value disables it.

WARNING: At the moment there is a small incompatibility with
tuned and NetworkManager which will results in connectivity
loss if you are using tuned and your network devices are
controlled by NetworkManager. This will be fixed in a future
version of tuned and NetworkManager. At the moment there
are two workarounds:

1) Don't use NetworkManager for your Ethernet devices but
configure them with system-config-network and enable the
network service.
2) Disabled the network tuning in tuned by editing
/etc/tuned.conf and uncomment the
# enabled=False
line in the NetTuning section.
